\changetocdepth {2}
\select@language {catalan}
\contentsline {chapter}{Sumari}{i}{section*.1}
\contentsline {chapter}{Acr\`onims}{iii}{chapter*.2}
\contentsline {chapter}{Resum}{v}{chapter*.3}
\contentsline {chapter}{\chapternumberline {1}Introducci\'o}{1}{chapter.1}
\contentsline {chapter}{\chapternumberline {2}Instruccions generals i itinerari del Treball Final de Grau }{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Introducci\'o}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Inici del TFG}{3}{section.2.2}
\contentsline {section}{\numberline {2.3}Selecci\'o del tema de TFG}{4}{section.2.3}
\contentsline {section}{\numberline {2.4}Inscripci\'o de la sol\IeC {\textperiodcentered }licitud de TFG i contracte docent}{4}{section.2.4}
\contentsline {section}{\numberline {2.5}Desenvolupament del TFG}{4}{section.2.5}
\contentsline {section}{\numberline {2.6}Dip\`osit de la mem\`oria}{5}{section.2.6}
\contentsline {section}{\numberline {2.7}Preparaci\'o de la presentaci\'o}{5}{section.2.7}
\contentsline {chapter}{\chapternumberline {3}La mem\`oria del treball de final de grau}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Principis b\`asics}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Bones pr\`actiques}{8}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Focalitzaci\'o}{8}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Elaboraci\'o de l'esb\'os}{9}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Redacci\'o}{10}{subsection.3.2.3}
\contentsline {section}{\numberline {3.3}Estructura del TFG}{11}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Portada}{11}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Taula de continguts o \'Index}{11}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Llista de figures}{12}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Llista de taules}{12}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Llista d'acr\`onims}{12}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Resum}{12}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}Agra\IeC {\@umlaut \i }ments}{13}{subsection.3.3.7}
\contentsline {subsection}{\numberline {3.3.8}Introducci\'o}{13}{subsection.3.3.8}
\contentsline {subsection}{\numberline {3.3.9}Desenvolupament}{14}{subsection.3.3.9}
\contentsline {subsection}{\numberline {3.3.10}Resultats i discussi\'o}{14}{subsection.3.3.10}
\contentsline {subsection}{\numberline {3.3.11}Conclusions}{15}{subsection.3.3.11}
\contentsline {subsection}{\numberline {3.3.12}Ap\`endixs}{15}{subsection.3.3.12}
\contentsline {subsection}{\numberline {3.3.13}Refer\`encies bibliogr\`afiques}{16}{subsection.3.3.13}
\contentsline {chapter}{\chapternumberline {4}La presentaci\'o del Treball Final de Grau}{17}{chapter.4}
\contentsline {section}{\numberline {4.1}Principis b\`asics}{17}{section.4.1}
\contentsline {section}{\numberline {4.2}Bones pr\`actiques}{18}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Les transpar\`encies}{18}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}L'exposici\'o i la defensa}{19}{subsection.4.2.2}
\contentsline {section}{\numberline {4.3}Estructura de la presentaci\'o del Treball Final de Grau}{20}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Portada}{20}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}\'Index}{20}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Desenvolupament}{20}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Resultats}{20}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Conclusions}{21}{subsection.4.3.5}
\contentsline {chapter}{\chapternumberline {5}Conclusions}{23}{chapter.5}
\contentsline {appendix}{\chapternumberline {A}Format final}{25}{appendix.A}
\contentsline {section}{\numberline {A.1}Suport electr\`onic}{25}{section.A.1}
\contentsline {section}{\numberline {A.2}Paper, impressi\'o i enquadernaci\'o}{25}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}Paper}{25}{subsection.A.2.1}
\contentsline {subsection}{\numberline {A.2.2}Impressi\'o a dues cares}{25}{subsection.A.2.2}
\contentsline {subsection}{\numberline {A.2.3}Enquadernaci\'o}{26}{subsection.A.2.3}
\contentsline {section}{\numberline {A.3}Format del document}{26}{section.A.3}
\contentsline {subsection}{\numberline {A.3.1}la plantilla de \textlatin {\LaTeX }}{26}{subsection.A.3.1}
\contentsline {section}{\numberline {A.4}F\'ormules, figures i taules}{27}{section.A.4}
\contentsline {subsection}{\numberline {A.4.1}F\'ormules}{27}{subsection.A.4.1}
\contentsline {subsection}{\numberline {A.4.2}Figures}{27}{subsection.A.4.2}
\contentsline {subsection}{\numberline {A.4.3}Taules}{28}{subsection.A.4.3}
\contentsline {section}{\numberline {A.5}Bibliografia}{29}{section.A.5}
\contentsline {section}{\numberline {A.6}Acr\`onims}{29}{section.A.6}
\contentsline {chapter}{Bibliografia}{31}{section*.8}
